package com.samebug.school

object AboutPartialFunctions extends App {

  val medalName1: PartialFunction[Int, String] = {
    case 1 => "Gold"
    case 2 => "Silver"
    case 3 => "Bronze"
  }

  println(medalName1(1))
  println(medalName1.isDefinedAt(-1))

  val medalName2: PartialFunction[Int, String] = {
    case position if 1 <= position && position <= 3 => Seq("Gold", "Silver", "Bronze")(position - 1)
  }

  println(medalName2(1))
  println(medalName2.isDefinedAt(-1))

  val medalName3 = new PartialFunction[Int, String] {
    override def isDefinedAt(position: Int): Boolean = 1 <= position && position <= 3
    override def apply(position: Int): String = Seq("Gold", "Silver", "Bronze")(position - 1)
  }

  println(medalName3(1))
  println(medalName3.isDefinedAt(-1))
}
